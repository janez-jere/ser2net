from fabric import task
import serial
from serial.tools import list_ports
from ser2net import server

@task
def ports(c):
    'Available ports'
    for c in list_ports.comports():
        print(c.device, '\t', c.description)


@task(name='server')
def run_server(c, port=4020, dev='COM1', baudrate=9600):
    'Run net server and comunicate with serial port'
    ser = serial.Serial(port=dev, baudrate=baudrate)
    server(port, ser)
