from functools import partial
import socket
import sys
import threading
from serial import Serial


class Socket:
    def __init__(self, s: socket.socket):
        self.s = s
        self.connected = True

    def write(self, d):
        if d:
            print('>', d)
            self.s.send(d)

    def read(self):
        sys.stdout.flush()
        d = self.s.recv(1024)
        if d:
            print('<', d)            
        else:
            print('Closing connection', self.s)
            self.connected = False
            self.s.close()
        return d
            

received = []

def get_client(port) -> socket.socket:
    host = '0.0.0.0'

    server_socket = socket.socket() 
    server_socket.bind((host, port))
    server_socket.listen(1)

    conn, address = server_socket.accept()
    print(f'New connection{address}')
    server_socket.close()
    return Socket(conn)


def s2n(c: Socket, ser: Serial):    
    'copy from serial to socket'
    while c.connected:
        c.write(ser.read(200))
        

def n2s(c: Socket, ser: Serial):    
    while c.connected:
        d = c.read()
        if d:
            received.append(d)
            ser.write(d)


def handle(port, ser: Serial):
    sys.stdout.flush()
    print('\nWaiting for client')
    c = get_client(port)
    ser.timeout = 3
    threading.Thread(target=partial(s2n, c, ser), daemon=True, name='s2n').start()
    threading.Thread(target=partial(n2s, c, ser), daemon=True, name='n2s').start()
    return c


def server(port=4020, ser: Serial=None):
    if not ser:
        ser = globals()['ser']
    while True:
        handle(port, ser)


def tserver():
    threading.Thread(target=server, daemon=True, name='server').start()


if __name__=='__main__':
    try:
        ser = Serial(port='COM1', baudrate=9600, timeout=3)
    except Exception as e:
        print(e)
    print(ser)
