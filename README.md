# Access serial port over net

Small python app

Install all and listen on port 4020 and COM1

``` bash
pipenv install
pipenv shell
fab -l
fab ports
fab server
```

## server options

```bash
fab -h server
#  -b INT, --baudrate=INT        
#  -d STRING, --dev=STRING       
#  -p INT, --port=INT

# listen on 4000, baudrate 19200, serial COM2
fab server -d COM2 -b 19200 -p 4000
```


